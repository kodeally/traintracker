<?php
/*
 * TrainTrackAPI to handle requests for stations and traintimes.
 * 
 */
define ('DOCUMENT_ROOT', dirname(__FILE__)); 
include(DOCUMENT_ROOT.'/src/Controller.php');

$controller = new Controller($_SERVER['REQUEST_URI']);
echo $controller->executeAction();

?>