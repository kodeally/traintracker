<?php 

include(DOCUMENT_ROOT.'/src/RailTimetableReaderInterface.php');
include(DOCUMENT_ROOT.'/src/AbstractTimeTableReader.php');

/*
 * An implementation of the RailTimetableReaderInterface for the Irish Rail.
 */
class IrishRailTimetableReader extends AbstractTimetableReader implements RailTimetableReaderInterface {

	function getStations($longitude = null, $latitude = null, $range = null) {
		$url = 'http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML';
		$data = $this->readUrlSource($url, array());
		if (!is_null($data)) {
			$data = $this->filterAndSortStations($data, $longitude, $latitude, $range);
			return $this->ConvertArrayToJson($data);	
		}
		return null;
	}
	
	function getTrainTimes($stationId, $timeInterval) {
		
		$url = 'http://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML_WithNumMins?StationCode=%s&NumMins=%s';
		$url = sprintf($url, $stationId, $timeInterval);
		$data = $this->readUrlSource($url, array());
		if (!is_null($data)) {
			$data = $this->filterAndSortTrainTimes($data, 10);
			return $this->ConvertArrayToJson($data);
		}
		return null;
	}
	
	function filterAndSortStations($data, $longitude, $latitude, $range) {
		$filteredStations = array();
		
		$applyDistanceCheck = false;
		if (!is_null($longitude) && !is_null($latitude) && !is_null($range)) {
			$applyDistanceCheck = true;
		}
		
		if ($data) {
			$stations = simplexml_load_string($data);
			foreach($stations->objStation as $station)
			{
				if (!is_null($longitude) && !is_null($latitude) && !is_null($range)) {
					$distance = $this->getGEODistance($this->convertValue($latitude), $this->convertValue($longitude), $this->convertValue($station->StationLatitude), $this->convertValue($station->StationLongitude), "k");
					if ($distance <= $range) {
						$filteredStations[] = array('StationId' => (string)$station->StationId, 'StationCode' => (string)$station->StationCode, 'StationDesc' => (string)$station->StationDesc, 'StationLongitude' => (string)$station->StationLongitude, 'StationLatitude' => (string)$station->StationLatitude, 'StationDistance' => $this->formatDistance($distance));
					}
				}
				else {
					$filteredStations[] = array('StationId' => (string)$station->StationId, 'StationCode' => (string)$station->StationCode, 'StationDesc' => (string)$station->StationDesc, 'StationLongitude' => (string)$station->StationLongitude, 'StationLatitude' => (string)$station->StationLatitude, 'StationDistance' => null);
				}
			}
		}
		
		if ($applyDistanceCheck) {
			usort( $filteredStations, 'stationSortByDistance' );
		}
		else {
			usort( $filteredStations, 'stationSortByStationDesc' );
		}
		
		return $filteredStations; 
	}	
	
	function filterAndSortTrainTimes($data, $limit) {
		$filteredTrains = array();
	
		$count = 0;
		if ($data) {
			$trains = simplexml_load_string($data);
			foreach($trains->objStationData as $train)
			{
				$filteredTrains[] = array('Traincode' => (string)$train->Traincode, 'Origin' => (string)$train->Origin, 'Destination' => (string)$train->Destination, 'Origintime' => (string)$train->Origintime, 'Destinationtime' => (string)$train->Destinationtime, 'Duein' => (string)$train->Duein, 'Late' => (string)$train->Late);
				$count++;
				if ($count == $limit) {
					break;
				}
			}
		}
	
		usort( $filteredTrains, 'trainsSortByTime' );
		
		return $filteredTrains;
	}
}

?>