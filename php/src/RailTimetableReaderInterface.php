<?php 

/*
 * A common interface for Timetable readers 
 */
interface RailTimetableReaderInterface {
	function getStations($longitude = null, $latitude = null, $range = null);
	function getTrainTimes($stationId, $timeInterval);
}

?>