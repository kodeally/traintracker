<?php 
/*
 * Base class for all Timetable readers
 * 
 */
abstract class AbstractTimetableReader {
	
	function readUrlSource($url, $parameters) {
		$options = array(
				'http' => array(
						'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
						'method'  => 'GET',
						'content' => http_build_query($parameters),
				),
		);
		$context  = stream_context_create($options);
		$result = file_get_contents($url, false, $context);
		return $result; 
	}
	
	function ConvertXmlToJson ($data) {
		$data = str_replace(array("\n", "\r", "\t"), '', $data);
		$data = trim(str_replace('"', "'", $data));
		$Xml = simplexml_load_string($data);
		$json = json_encode($Xml);
		return $json;
	}
	
	function ConvertArrayToJson ($data) {
		$json = json_encode($data);
		return $json;
	}
	
	function convertValue($value) {
		if (!is_null($value)) {
			return doubleval($value);
		}
		return null;
	}
	
	function formatDistance($distance) {
		if (!is_null($distance)) {
			return round($distance, 2);
		}
		return null;
	}
	
	/*
	 * Taken from http://chrishacia.com/2012/03/php-calculate-distance-between-2-longitude-latitude-points/
	*/
	function getGEODistance($lat1, $lon1, $lat2, $lon2, $unit) {
		if (!is_null($lat1) && !is_null($lon1) && !is_null($lat2) && !is_null($lon2) && !is_null($unit)) {
			$theta = $lon1 - $lon2;
			$dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
			$dist = acos($dist);
			$dist = rad2deg($dist);
			$miles = $dist * 60 * 1.1515;
			$unit = strtoupper($unit);
			
			if($unit == "K")
			{
				return ($miles * 1.609344);
			}
			elseif($unit == "N") {
				return ($miles * 0.8684);
			}
			else
			{
				return $miles;
			}	
		}
		return null;
	}
}

function stationSortByStationDesc( $a, $b ) {
	return strcmp($a['StationDesc'], $b['StationDesc']);
}

function stationSortByDistance( $a, $b ) {
	return $a['StationDistance'] == $b['StationDistance'] ? 0 : ( $a['StationDistance'] > $b['StationDistance'] ) ? 1 : -1;
}

function trainsSortByTime( $a, $b ) {
	return $a['Duein'] == $b['Duein'] ? 0 : ( $a['Duein'] > $b['Duein'] ) ? 1 : -1;
}


?>