<?php 
include(DOCUMENT_ROOT.'/src/IrishRailTimetableReader.php');

/*
 * Basic controller to handle requests i.e Actions
 *  - getStationsAction
 *  - getTrainTimesAction
 */
class Controller {
	
	protected $action;
	protected $parameters;
	
	public function __construct($url){
		$this->parameters = array();
		
		if (!is_null($url)) {
			$urlInfo = parse_url($url);
			$urlParameters = explode('&', $urlInfo['query']);
			
			foreach($urlParameters as $parameter) {
				$parts = explode('=', $parameter);
			
				if (!is_null($parts) && count($parts) == 2) {
					if (strcmp($parts[0],'action') == 0) {
						$this->action = $parts[1];
					}
					else {
						$this->parameters[$parts[0]] = $parts[1];
					}
				}
			}	
		}
	}
	
	/*
	 * This method just returns the Irish Rail Reader but it could be changed to return different readers 
	 * based on the user location e.g UK Timetable reader if they were in the UK.
	 */
	function getTimetableReader() {
		return new IrishRailTimetableReader();
	}
	
	function executeAction() {
		$actionFunction = $this->action . 'Action'; 
		if (method_exists($this, $actionFunction)) {
			return $this->$actionFunction($this->parameters);
		}
		else {
			echo $actionFunction . " is nt a vaild action.";
		}
	}
	
	function validParameter($parameters, $name) {
		if (isset($parameters[$name]) && strlen($parameters[$name]) > 0) {
			return true;
		}
		return false;
	}
	
	function getStationsAction($parameters) {
		$reader = $this->getTimetableReader();
		if (!is_null($parameters) && (count($parameters) == 3 && $this->validParameter($parameters, 'longitude') && $this->validParameter($parameters, 'latitude') && $this->validParameter($parameters, 'range'))) {
			return $reader->getStations($parameters['longitude'], $parameters['latitude'], $parameters['range']);
		}
		else {
			return $reader->getStations();
		}
	}
	
	function getTrainTimesAction($parameters) {
		$reader = $this->getTimetableReader();
		if (!is_null($parameters) && count($parameters) == 2 && isset($parameters['stationId']) && isset($parameters['timeInterval'])) {
			return $reader->getTrainTimes($parameters['stationId'], $parameters['timeInterval']);
		}
		return null;
	}
}

?>