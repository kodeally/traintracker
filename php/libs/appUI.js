/*
 * Event functions
 */

jq(document).delegate('#back','click', function (ev) {
	navigateHistory('back');
});

jq(document).delegate('#map','click', function (ev) {	
	jQuery.when(loadMap()).then(function(trainTimes) {
		showPage('#Map');
	});
});

jq(document).delegate('#select_list_ul li','click', function (ev) {
	var listItem = jq(this);
	var link = listItem.children();
	var id = link.attr("data-id");
	var type = link.attr("data-type");
	
	displayListItem(id, type)
});

/*
 * End Event functions
 */

/*
 * Display functions
 */

function loadMap() {
	var deferred = jQuery.Deferred();

	jQuery.when(getCurrentLocation()).then(function(longitude, latitude) {
		var position = new google.maps.LatLng(latitude,longitude);
		
		var mapOptions = {
	    	zoom: 10,
	        center: position,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        streetViewControl: false,
	        mapTypeControl: false,
	    }
	    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

		//http://mapicons.nicolasmollet.com/category/markers/transportation/
		var marker = new google.maps.Marker({
			position: position,
			map: map,
			icon: iconBase + 'person.png',
		    title: 'Me'
		});
		
		var station = appStations.getCurrentStation();
		if (station) {
			var stationPosition = new google.maps.LatLng(station.StationLatitude,station.StationLongitude);
			var marker = new google.maps.Marker({
				position: stationPosition,
				map: map,
				icon: iconBase + 'train.png',
			    title: 'Train'
			});
		}
		
	});
	 
	return deferred.resolve();	
}

function showPage(div, animation) {
	manageHistory(div);
	
	var animationEffect = "slide";
	if (animationEffect) {
		animation = animationEffect;
	}
	
	jq.ui.loadContent(div,false,false,"slide");
	
	if (div.indexOf('edit') > 0 || div.indexOf('login') > 0 || div.indexOf('SystemType_list_select') > 0) {
		jq.ui.toggleNavMenu(false);
	}
	else {
		jq.ui.showNavMenu = true;
		jq.ui.toggleNavMenu(true);		
	}
}

function displayListItem(id, type) {

	var station = appStations.getStation(id);
	appStations.setCurrentStation(station);
	
	if (station) {
		var item = jq.template('Station_template', {data:station});
		jq('#Station_view #info').empty();
		jq('#Station_view #info').append(item);
		
		jq('#Station_view #Train_Time_select_list_ul').empty();
		jQuery.when(loadTrainTimes(id,timeInterval)).then(function(trainTimes) {
			if (trainTimes && trainTimes.length > 0) {
				jQuery(trainTimes).each(function(index, train) {
					formatTrainInformation(train);
					var item = jq.template('Train_Times_list_template', {data:train});
					jq('#Station_view #Train_Time_select_list_ul').append(item);
				});
			}
			showPage('Station_view');
		});
	}
}

function formatTrainInformation(train) {
	if (train.Late && train.Late > 0) {
		train.Duein = train.Duein + "min (" + train.Late + " min late) ";
	}
	else {
		train.Duein = train.Duein + "min";
	}
}

function displayStations(stations) {
	if (stations) {
		jQuery(stations).each(function(index, station) {
			formatStationInformation(station);
			var item = jq.template('Stations_list_template', {data:station});
			jq('#Stations_list #select_list_ul').append(item);
		});
	}
}

function formatStationInformation(station) {
	if (station.StationDistance && station.StationDistance > 0) {
		station.StationFullDescription = station.StationDesc + " (" +  station.StationDistance + ")KM";
	}
	else {
		station.StationFullDescription = station.StationDesc;
	}
}

/*
 * End Display functions
 */


/*
 * History functions
 */
function manageHistory(page) {
	if (page == '#home') {
		g_history.length = 0;
	}
	else {
		g_history.push(page);
	}
}

function navigateHistory(direction) {
	var page = '#home';
	
	g_history.pop();
	
	if (g_history.length > 0) {
		page = g_history[g_history.length-1];
		g_history.pop();
	}
	
	/*
	 * If this is an edit page then pop this to and go to the next page.
	 * This is because a new entity does not an id and click save would create a duplicate. 
	 */
	if (page.indexOf('edit') > -1) {
		page = g_history[g_history.length-1];
		g_history.pop();
		/*
		 * If the next page is a SystemType_list_select then skip it give them the entity list.
		 * i.e Dont display the Cow, Bull page list give them the animals page.
		 */		
		if (page.indexOf('SystemType_list_select') > -1) {
			page = g_history[g_history.length-1];
			g_history.pop();
		}
	}
	showPage(page);
}

/*
 * End History functions
 */