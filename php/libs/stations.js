
function Stations() {
	this.stations;
	this.currentStation;
}

Stations.prototype.getCurrentStation = function() {
	return this.currentStation;
};

Stations.prototype.setCurrentStation = function(currentStation) {
	this.currentStation = currentStation;
};

Stations.prototype.getStations = function() {
	return this.stations;
};

Stations.prototype.setStations = function(stations) {
	this.stations = stations;
};

Stations.prototype.getStation = function(code) {
	var objects = this.getObjects(this.stations, 'StationCode', code);
	if (objects && objects.length == 1) {
		return objects[0];
	}
};

Stations.prototype.getObjects = function(obj, key, val) {
	var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(this.getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}
