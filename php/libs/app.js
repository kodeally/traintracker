		var app_name = "Train Track";
		var g_history = new Array();
		var iconBase = document.URL + '/images/';
		
		/* This function runs once the page is loaded, but appMobi is not yet active */
        $.ui.autoLaunch=false;
		$.ui.useOSThemes=false; //For Appframework v2.0+ This must be set before $(document).ready() triggers;
        
		/* This function runs once the page is loaded, but appMobi is not yet active */
        jq.ui.autoLaunch=false;
        var init = function(){		
			window.setTimeout(function(){jq.ui.launch();},1500);
			jq.ui.ready(function(){
				logMessage('DEBUG', "jqUi is now Ready");
				runApp();
			});
        };
		
        document.addEventListener("DOMContentLoaded",init,false);
		
        function runApp() {
			jq.ui.ready(function(){
				logMessage('DEBUG', "Start app.");
				
				jQuery.when(loadStationData(100)).then(function(result) {
					showPage("#Stations_list");
				});
			});
		}
		
		function logMessage(level, message, error) {
			
			if (error) {
				if (error.code) {
					message = message + " " + error.code + ",  " + error.message;
        		}
        		else {
        			message = message + " " + error;
        		}
			}
        		
			if (level == 'ERROR') {
				console.error(level + ' ' + message);
			}
			else if (level == 'WARN') {
				console.warn(level + ' ' + message);
			}
			else {
				console.log(level + ' ' + message);
			}
		}