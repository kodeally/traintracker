var appStations = new Stations();
var timeInterval = '20';

function getCurrentLocation() {
	var deferred = jQuery.Deferred();
	
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(function(position) {
			deferred.resolve(position.coords.longitude, position.coords.latitude);
		},
		function() {
			deferred.resolve(null, null);
		});
	}
	else {
		deferred.resolve(null, null);
	}
	
	return deferred;
}
	    


function loadStationData(range) {
	var deferred = jQuery.Deferred();
	
	jQuery.when(getCurrentLocation()).then(function(longitude, latitude) {
		jQuery.when(loadStations(longitude, latitude, range)).then(function(stations) {
			if (stations) {
				appStations.setStations(stations);
				logMessage('DEBUG', stations.length  + ' stations loaded');
		    	displayStations(appStations.getStations());
		    	deferred.resolve();
			}
			else {
				deferred.resolve();
			}
		});
	});
	
	return deferred;
}

function loadStations(appLongitude, appLatitude, appRange) {
	var deferred = jQuery.Deferred();
	logMessage('DEBUG', 'Loading station data from server with longitude ' + appLongitude + ' latitude ' + appLatitude + ' range ' + appRange);

	$.ui.showMask('Loading Station data');
	
	var stations = null;
	
	var request = jQuery.ajax({
		type: "GET",
		dataType: "json",
		url: 'trainTrackApi.php',
		data: { action: "getStations", longitude: appLongitude, latitude: appLatitude, range: appRange }
	});
	
	// callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
    	stations = response;
    });

    request.always(function() {
    	$.ui.hideMask();
    	deferred.resolve(stations);
    });
    
	return deferred;	
}

function loadTrainTimes(stationId, timeInterval) {
	var deferred = jQuery.Deferred();
	logMessage('DEBUG', 'Loading timetable');

	$.ui.showMask('Loading Station data');
	
	var trainTimes = null;
	
	var request = jQuery.ajax({
		type: "GET",
		dataType: "json",
		url: 'trainTrackApi.php',
	    data: { 'action': "getTrainTimes", 'stationId': stationId, 'timeInterval': timeInterval }	
	});

    request.done(function (response, textStatus, jqXHR){
    	if (response) {
    		trainTimes = response;
    	}
    });

    request.always(function() {
    	$.ui.hideMask();
    	deferred.resolve(trainTimes);
    });
    
	return deferred;	
}